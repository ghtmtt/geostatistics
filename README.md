## Geostatistic R-Script for QGIS

This repository contains both manual and code for geostatistical analysis in 
QGIS.

The process involves QGIS and R. Scripts are written in a *hybrid* code language
that is understood by the native **Processing** plugin of QGIS. 

Each algorithm has to be run and used within QGIS and they *call* an instance of
R that runs the code while the results will be loaded (or showed) in QGIS.

The repository is divided in 2 folders:

1. **manual** with both Italian and English manual instruction and detailed
description of the code

2. **rscripts** with all the algorithms of the geostatistical analysis


## Credits

The script suite has been developed by Matteo Ghetta within the [Freewat Project H2020](http://www.freewat.eu/)
coordinated by Rudy Rossetto of the [Scuola Superiore Sant'Anna of Pisa](https://www.santannapisa.it/).

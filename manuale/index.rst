.. Geostatistica in QGIS & R
  Matteo Ghetta

Interpolation methods in QGIS
=============================

.. toctree::
   :maxdepth: 3

   source/geostatistica
   source/interpolazione
   source/installazione
   source/statistica_descrittiva
   source/vario_kriging
   source/installazioneR.rst
   source/A1_termini_geostatistica
   source/A2_installazione_pacchetti
   source/A3_altri_strumenti

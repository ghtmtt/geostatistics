��          T               �   P   �   �   �   �   `     �  �   �  \   �  �    P   �  �   �  �   l     �  �   �  \   �   I dati cosi ottenuti possono essere inseriti come input nel calcolo del kriging. Il **kriging** è un metodo di interpolazione statistico, ovvero il suo utilizzo prevede un'attenta analisi della zona di studio. Il kriging infatti necessita di alcuni parametri in input che devono essere calcolati direttamente dai dati della zona di studio. Kriging Ovvero prima si calcola la semivarianza per ogni coppia di punti (Variogramm Cloud) poi si calcola il semi variogramma per un determinato range di distanza (lag) e infine si calcola il variogramma modello. Questi parametri vengono estrapolati dal variogramma sperimentale e dal varigoramma modello. Project-Id-Version: Interpolation methods in QGIS 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-15 12:23+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 I dati cosi ottenuti possono essere inseriti come input nel calcolo del kriging. Il **kriging** è un metodo di interpolazione statistico, ovvero il suo utilizzo prevede un'attenta analisi della zona di studio. Il kriging infatti necessita di alcuni parametri in input che devono essere calcolati direttamente dai dati della zona di studio. Kriging Ovvero prima si calcola la semivarianza per ogni coppia di punti (Variogramm Cloud) poi si calcola il semi variogramma per un determinato range di distanza (lag) e infine si calcola il variogramma modello. Questi parametri vengono estrapolati dal variogramma sperimentale e dal varigoramma modello. 
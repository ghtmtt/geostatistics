.. _vario_kriging:

Variogramma e Kriging
=====================
La teoria e l'importanza del variogramma è stata affrontata nel capitolo :ref:`variogramma`. Infatti prima di poter effettuare un kriging che abbia significato, è necessario studiare a fondo il comportamento spaziale della variabile in modo da fornire i giusti dati in input per il kriging e per minimizzare gli errori.

Nei metodi di interpolazione deterministici infatti il *peso* dell'interpolazione è fornito intrinsecamente dai dati, nel *kriging* invece il peso viene ricavato dal variogramma.

Variogramma
-----------
Nel set di script fornito, molti sono dedicati al variogramma nelle sue molteplici diversificazioni.

Variogram cloud
"""""""""""""""
Un modo molto rapido (ma poco interpretabile) per studiare l'autocorrelazione spaziale è la *variogram cloud*. L'algoritmo ``VariogramCloudGG`` permette di visualizzare rapidamente i risultati per la variabile selezionata.

.. figure:: ../immagini/vario_kriging/variocloud.*
  :scale: 80%
  :align: center


.. figure:: ../immagini/vario_kriging/variocloud2.*
  :scale: 80%
  :align: center


Variogramma sperimentale (no direzione)
"""""""""""""""""""""""""""""""""""""""
Quando **non** si tiene conto della variabilità in specifiche direzioni di una variabile si sottintende un comportamento **isotropo**, ovvero uguale in ogni direzione.
L'algoritmo ``VariogramOneDirection`` permette di scegliere una variabile e di creare il rispettivo variogramma sperimentale.

.. figure:: ../immagini/vario_kriging/VariogramOneDirection.*
  :scale: 80%
  :align: center

Nel grafico finale vengono mostrati i punti e il conteggio di tutti i campioni **per ogni lag**.

.. figure:: ../immagini/vario_kriging/VariogramOneDirection2.*
  :scale: 80%
  :align: center

.. _variogramma_anisotropia:

Variogramma sperimentale (multi direzionale)
""""""""""""""""""""""""""""""""""""""""""""
Se si vuole tenere in considerazione un comportamente diverso della variabile in alcune direzioni principali si sta lavorando in un ambiente **anisotropo** (è il caso per esempio di un comportamento simile in una direzione nel caso della presenza di un fiume).

L'algoritmo ``VariogramDirections`` permette, oltre ad inserire la variabile di cui si vuole studiare il comportamento, anche 4 direzioni preferenziali. Le direzioni preferenziali (opzionali, è possibile inserirne anche una sola), sono espresse in gradi numerici, ovvero il Nord = 0 e i numeri crescono in senso orario (Est = 90, Sud = 180, Ovest = 270).

.. figure:: ../immagini/vario_kriging/VariogramDirections3.*
  :scale: 50%
  :align: center

.. warning:: Inserire 0 o 180 è la stessa cosa! Infatti inserendo 0 si intende una direzione preferenziale Nord-Sud, inserendo 180 Sud-Nord che in termini matematici è la stessa cosa

.. figure:: ../immagini/vario_kriging/VariogramDirections.*
  :scale: 80%
  :align: center

Il grafico finale mostra il variogramma sperimentale con le direzioni in colori diversi (presenti in legenda)

.. figure:: ../immagini/vario_kriging/VariogramDirections2.*
  :scale: 80%
  :align: center


Superficie del variogramma (variogram surface)
""""""""""""""""""""""""""""""""""""""""""""""
L'algoritmo ``VariogramMap`` crea un grafico con i valori di varianza della variabile selezionata.
Il grafico permette di comprovare le principali direzioni di anisotropia viste con il :ref:`variogramma_anisotropia`.

Dopo aver scelto il layer e la variabile è necessario inserire un valore di ``cutoff`` (distanza massima) e di ``width`` (valore del lag)

.. figure:: ../immagini/vario_kriging/VariogramMap.*
  :scale: 80%
  :align: center

Il grafico che si ottiene simula una mappa con i valori di varianza evidenziati da un gradiente di colori (verde valori bassi, marrone valori alti) dove il bianco corrisponde a valori di varianza nulli.

.. figure:: ../immagini/vario_kriging/VariogramMap2.*
  :scale: 80%
  :align: center


Modello del Variogramma
"""""""""""""""""""""""
Il modello del variogramma è un *fitting* del variogramma sperimentale (vedi :ref:`modello_variogramma`). L'algoritmo ``VariogramModel`` permette di calcolare in modo molto semplice il modello del variogramma e di estrapolare i parametri utili per l'interpolazione.

L'algoritmo permette di inserire manualmente, oltre al layer e alla variabile, anche il tipo di modello (4 modelli disponibili). Inoltre i parametri di ``nugget``, ``psill`` e ``range`` possono essere inseriti manualmente (spuntando la casella di controllo e inserendo manualmente i valori) oppure si può decidere di lasciarli calcolare all'algoritmo.

.. figure:: ../immagini/vario_kriging/VariogramModel.*
  :scale: 80%
  :align: center

L'algoritmo restituisce 2 output:

1. un breve riassunto dei parametri del modello (tipo modello, valori di range e psill)
2. il grafico del variogramma modello con la linea di fitting e i valori dei punti a diversi lag

  .. figure:: ../immagini/vario_kriging/VariogramModel2.*
    :scale: 80%
    :align: center


Cross Validation
----------------
L'algoritmo ``CrossValidation`` serve per validare i risultati del variogramma. Come per il variogramma modello, in questo algoritmo si possono inserire manualmente i valori di *nugget*, *psill* e *range* oppure lasciare che sia l'algoritmo a calcolarli.

.. figure:: ../immagini/vario_kriging/CrossValidation.*
  :scale: 80%
  :align: center

In uscita l'algoritmo restituisce un insieme di 4 grafici:

1. Valori predetti contro *zscore*
2. istogramma di *zscore*
3. valori osservati contro valori predetti
4. mappa dei punti con valori *estremi*

.. figure:: ../immagini/vario_kriging/CrossValidation2.*
  :scale: 80%
  :align: center

L'algoritmo restituisce anche 2 output da console:

1. media degli errori standard (idealmente 0)
2. varianza degli errori standard (idealmente 1)

.. figure:: ../immagini/vario_kriging/CrossValidation3.*
  :scale: 80%
  :align: center


.. _ordinary_kriging:

Kriging Ordinario
-----------------
Dopo aver studiato i variogrammi, gli errori associati e dopo aver validato i risultati, è possibile proseguire con il kriging.
A differenza degli altri interpolatori, il kriging, oltre alla superficie di valori predetti, restituisce anche la superficie degli errori stimati (tramite la misura della varianza).
In questo modo è possibile capire quali siano le zone dove la stima è *meno corretta* e quindi dove l'errore è maggiore.

L'algoritmo ``OrdinaryKriging2`` permette,  in modo semplice, di effetture un kriging ordinario di una variabile. Come per il variogramma modello è possibile inserire manualmente i valori di *nugget*, *psill* e *range*.

.. figure:: ../immagini/vario_kriging/OrdinaryKriging.*
  :scale: 80%
  :align: center

Inoltre l'algoritmo permette di inserire anche l'estensione della mappa del kriging finale. Dal parametro **Extent** è sufficiente cliccare sul pulsante |browse| per aprire un menu a tendina ed effettuare 4 scelte diverse:

1. usare l'estensione minima del vettore in input (scelta predefinita)
2. scegliere l'estensione da un layer presente in legenda
3. selezionare manualmente l'estensione dalla mappa
4. inserire manualmente i 4 valori dell'estensione

.. figure:: ../immagini/vario_kriging/OrdinaryKriging2.*
  :scale: 80%
  :align: center

Oltre all'estensione è possibile scegliere la **dimensione del pixel** della mappa in uscita. Dal momento che l'input di questo parametro è un numero, è possibile cliccare sul pulsante |browse| e inserire un'espressione, oppure un valore automaticamente calcolato dai layer (vettori o raster) caricati in QGIS (per esempio, la dimensione del pixel di un raster). Nel seguente esempio si è scelto di caricare i valori della dimensione del pixel del raster *dtm* caricato in QGIS.

Per un approfondimento di come inserire espressioni, vedi :ref:`espressioni`.

.. note:: assicurarsi che nella scheda dell'espressione (3) compaia il parametro voluto e che l'anteprima del risultato (4) non presenti errori

.. figure:: ../immagini/vario_kriging/OrdinaryKriging3.*
  :scale: 80%
  :align: center

Scelti tutti i parametri è possibile avviare l'algoritmo che restituirà diversi output:

1. un grafico del varigramma modello sovrapposto al variogramma sperimentale

.. figure:: ../immagini/vario_kriging/OrdinaryKriging4.*
  :scale: 80%
  :align: center

2. un output da console con il variogramma modello scelto, i valori di psill e range calcolati (o inseriti manualmente)

.. figure:: ../immagini/vario_kriging/OrdinaryKriging5.*
  :scale: 80%
  :align: center

L'algoritmo crea anche 2 layer raster (proiettati nello stesso sistema di riferimento del vettore scelto per il kriging). Nelle seguenti figure è stato applicato uno stile manuale **dopo** che i dati sono stati creati.

3. la superficie dei valori predetti

.. figure:: ../immagini/vario_kriging/OrdinaryKriging6.*
  :scale: 80%
  :align: center

4. la superficie della varianza, ovvero delle zone di maggiore errore associate all'interpolazione

.. figure:: ../immagini/vario_kriging/OrdinaryKriging7.*
  :scale: 80%
  :align: center

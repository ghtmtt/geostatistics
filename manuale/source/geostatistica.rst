.. _geostatistica:

=============
Geostatistica
=============
La geostatistica è quella branca della statistica che si occupa dell'analisi e interpretazione di dati geografici.

* Come varia una variabile nello spazio?
* Che cosa controlla la sua variazione nello spazio?
* Quali e quanti campioni sono necessari per descrivere la sua variabilità spaziale?
* Qual è il valore della variabile in una specifica posizione (predizione)?
* Qual è l’incertezza di questa stima?


Sono dovuti passare molti anni prima che la geostatistica venisse applicata anche ad altri settore oltre a quello di esplorazione mineraria e petrolifera

* rappresentazione matematica complessa
* nessun sotware GIS fino agli anni '90
* costi dei software molto elevati

L’ambiente è un dominio continuo nelle tre dimensioni (e anche nel tempo) e possiamo tentare di effettuare misure solamente in un numero finito di siti, non si può pensare di campionare tutti i possibili punti di un'area.

Il compito della geostatistica è quindi quello di **stimare** i valori incogniti di una zona grazie alla conoscenza delle zone circostanti (un po' come si fa con l'inferenza statistica).

.. image:: ../immagini/geostatistica/geo_1.png
    :align: center

Visti in un'ottica spaziale, i campioni raccolti in campo sono descritti da una coppia di coordinate *x* e *y* e contengono anche informazioni su uno o più valori raccolti (altezza del punto rispetto al livello del mare, concentrazione di un inquinante, porosità del terreno, ecc..)

Si può quindi creare un grafico dei punti campionati che in aggiunta alle coordinate mostra anche le informazioni della variabile in oggetto, nell'esempio seguente la concentrazione di zinco viene rappresentata da una scala di colori.

Risulta cosi molto intuitivo capire il comportamento spaziale della variabile:

.. image:: ../immagini/geostatistica/geo_1_2.png
    :align: center

Se inizialmente la geostatistica si occupava solamente di resitutire i valori **interpolati** di un'area, la sua evoluzione ha permesso di misurare anche l'**incertezza** del dato, ovvero l'errore della stima.

Il valore interpolato **insieme** al suo valore d'incertezza permette una comprensione a tutto tondo delal zona studiata e dei valori stimati.


Autocorrelazione spaziale (dipendenza statistica)
=================================================
La prima legge di Tobler dice che tutto è in relazione con tutto, ma oggetti vicini sono più simili di oggetti lontani.

In altre parole l'autocorrelazione afferma che il valore del ci permette di capire se il valore dei punti è in qualche modo correlato anche alla distanza fra i punti stessi.

Un ottimo esempio per capire questo fenomeno è pensare ai valori di quota; zone adiacenti hanno valori di quota molto simili, punti lontani invece possono avere dei valori molto diversi e quindi **non** essere spazialmente correlati.

**La posizione del campione misurato è importante tanto quanto il valore stesso**


Questa differenza all'aumentare della distanza significa che i punti sono **spazialmente autocorrelati** proprio perché punti lontani sono meno simili di punti vicini.

.. image:: ../immagini/geostatistica/geo_2.jpg
    :align: center


Variogramma
===========
I concetti descritti soprano trovano soluzione matematica grazie al **variogramma**.

Il variogramma infatti è la funzione che descrive il grado di dipendenza spaziale di una variabile stocastica. In altre parole il variogramma riesce a spiegare quanto due campioni sono simili in funzione di quanto sono vicini.


.. _variocloud:

Variogram Cloud
===============
Uno dei primi strumenti che si possono utilizzare per capire le proprietà della variabili da studiare è la **variogram cloud**.

Per ogni punto viene calcolata la distanza  e la semivarianza rispetto a **tutti gli altri punti**. Considerando un sottoinsieme dei punti con le concentrazioni di zinco descritti sopra, si può immaginare come per ogni punto venga calcolata la distanza e la semivarianza della variabile rispetto a tutti gli altri punti:

.. image:: ../immagini/geostatistica/variocloud.png
    :align: center

I valori ottenuti vengono messi in grafico dove l'asse x rappresenta la distanza mentre l'asse y i valori di semivarianza, si ottiene una nuvola di punti, detta appunto *variogram cloud*.

.. image:: ../immagini/geostatistica/variocloud2.png
    :align: center

La variogram cloud permette di individuare eventuali outliers, ovvero punti che a distanze molto piccole prensentano grandi variabilità (fenomeno non infrequente in campo ambientale).

.. _expvariogram:

Variogramma sperimentale
========================
La *variogram cloud* è difficile da interpretare: infatti in situazioni normali la nuvola è molto fitta e con molti punti.

Il passo successivo per una migliore interpretazione è il *variogramma sperimentale*. In questo grafico infatti vengono rappresentat gli **scarti quadratici medi** a determinate distanze.

Per facilitare la comprensione della variabilità spaziale si possono prendere delle misure standard di distanza, dette **lag** e calcolare **un unico valore** di semivarianza per ogni distanza. Si ottengono cosi dei cerchi di raggio h all'interno dei quali viene calcolato un valore rappresentativo di semivarianza.


.. image:: ../immagini/geostatistica/variocloud3.png
    :align: center

All'aumentare del raggio del cerchio si avrà un corrispondente aumento della semivarianza (proprio perché più la distanza aumenta più aumenta la variabilità dei punti).

Il grafico che otterrò è chiamato **semi variogramma**: ogni punto rappresenta la sommatoria della semi varianza a cert intervalli di distanze, detti **lag**.

.. image:: ../immagini/geostatistica/expvario.png
    :align: center

Si vede come la semi varianza aumenti con la distanza, come giustamente ci si aspetta. Tuttavia, dopo una certa distanza, la semivarianza si *assesta* a un valore **limite**. Questo valore ci permette di capire qual è la distanza massima entro cui i punti hanno un comportamento simile, ovvero la distanza entro il quale i punti sono **spazialmente autocorrelati**.

.. _modello_variogramma:

Modello del variogramma
=======================
I dati e i valori visti nel semi variogramma sono valori *puntuali* riferiti ai punti della zona di studio.

Quello che è necessario è trovare un **modello continuo** che meglio si avvicini alla distribuzione dei punti, ovvero è necessario calcolare il **variogramma modello**.

Il variogramma modello permette quindi di avere un modello di predizione **continuo** della variabile studiata.

.. image:: ../immagini/geostatistica/geo_4.png
    :align: center

Nello schema sopra riportato, i singoli punti fanno riferimento al variogramma sperimentale, mentre la linea continua rappresenta il variogramma modello.

Il **variogramma modello** calcola 3 importanti variabili:

1. **range**, ovvero la **distanza** alla quale la semivarianza cessa di aumentare e i punti non sono più spazialmente autocorrelati (la varianza è sempre la stessa anche se la distanza aumenta)

2. il **nugget**, ovvero il valore della semivarianza a distanza nulla, che può corrispondere alla variabilità naturale di fondo, a un errore di campionamento o a condizioni particolari della zona

3. il **sill**, che corrisponde al valore della semivarianza quando il modello si assesta. Permette cioè di capire entro quale varianza *ha senso* effettuare un'interpolazione



Flusso di lavoro geostatistico
==============================
Un'analisi geostatistica prevede diversi step, ognuno dei quali importante per ottenere risultati spendibili.

1. si inizia con la raccolta dei dati e uno studio preliminare. Con questa prima analisi dei dati si può capire se ci sono valori anomali, valiri ripetuti e se è necessario provvedere a una trasformazione dei dati

2. i dati ora analizzati vengono pre-processati, ovvero viene valutato se togliere dei trend spaziali, se trasformare (normalizzare) la distribuzione dei valori ed effettuare un'analisi cluster per compensare dati di campionamento presi in maniera non ottimale

3. accanto al pre-processamento può essere necessario anche costruire un modello della struttura spaziale dei dati (correlazione spaziale). Alcuni di questi metodi, come il *kriging*, richiedono esplicitamente lo studio della correlazione spaziale (tramite il variogramma) **prima** di essere utilizzati, altri metodi invece, come la *distanza inversa ponderata*, richiedono un semplice parametro di struttura spaziale, ma basato sulla conoscenza a priori del fenomeno

4. in funzione di quanti punti sono disponibili è necessario capire qual è il minimo di questi punti necessario per stimare altri valori. Questo dato è necessario anche per capire il valore di incertezza del modello (usando 1000 punti in una piccola area ho sicuramente un valore di incertezza minore rispetto a 10 punti in un'area più estesa)

5. una volta definiti tutti questi parametri e punti, è possibile inserirli nel modello. Il risultato finale è di solito una mappa (raster) di valori continui (ovvero dei valori interpolati). A questo punto è possibile anche investigare eventuali valori anomali perché questi cambiano notevolmente il risultato finale.

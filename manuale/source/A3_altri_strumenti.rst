.. _altri_strumenti:

=====================
Altri strumenti utili
=====================

Trasformazione dati
-------------------
QGIS offre un modo molto semplice ed efficace per trasformare i dati (trasformazioni logaritmiche, esponenziali, ecc...).

Per trasformare una variabile, selezionare il layer cliccandoci sopra (1) (in modo che sia evidenziato di blu) e aprire il **Calcolatore di campi** (2):

.. figure:: ../immagini/A3_altri_strumenti/trans_data.*
  :scale: 80%
  :align: center

nella finestra seguente è possibile effettuare numerossissime operazioni sulle variabili del dataset con una sintassi molto semplice.

Le variabili possono essere sovrascritte oppure può essere creato un nuovo campo (scelta consigliata) e il filtro centrale permette di ricercare facilmente le funzioni raggruppate per argomento:

.. figure:: ../immagini/A3_altri_strumenti/trans_data2.*
  :scale: 80%
  :align: center

nel seguente esempio è stata create una nuova variabile, *Pb_log*, calcolata come logaritmo di base 10 della variabile originale Pb. Il nuovo campo è di tipo decimale con 2 numeri dopo la virgola:

.. figure:: ../immagini/A3_altri_strumenti/trans_data3.*
  :scale: 80%
  :align: center

Cliccando su ``OK`` la nuova variabile viene scritta nella tabella degli attributi:

.. figure:: ../immagini/A3_altri_strumenti/trans_data4.*
  :scale: 80%
  :align: center

Una guida completa dell'algoritmo è disponibile alla `pagina ufficiale di QGIS <http://docs.qgis.org/2.14/en/docs/user_manual/working_with_vector/expression.html#expressions>`_


.. _espressioni:

Uso avanzato dei parametri e delle variabili
--------------------------------------------
In ogni finestra di dialogo dove è possibile inserire un'espressione (Seleziona con espressione, calcolatore di campi, etc.), è presente un menu che permette di usare alcuni dati del layer caricati in QGIS.

I dati calcolati ed utilizzabili aumentano nel **Calcolatore di Campi** presente in ``Processing`` (vedi :ref:``ordinary_kriging``). Infatti in questi casi è possibile utilizzare un maggior numero di **Variabili**, appartenenti sia a QGIS (estensione della mappa, nome del progetto, versione di QGIS, etc.) oppure anche di **tutti** i layer caricati in QGIS (dimensione del pixel di un raster, valore di xmin di un vettore, valore medio della banda di un raster, etc.)

Questo menu prende il nome di **Variabili** ed è presente nella parte centrale del **Costruttore di Interrogazioni**:

Facendo doppio click sulla variabile desiderata, questa viene aggiunta all'interrogazione. Dal momento che alcune di queste variabili sono semplici numeri, è possibile effettuare operazioni matematiche semplici e complesse:

.. figure:: ../immagini/A3_altri_strumenti/Expression.*
  :scale: 80%
  :align: center

Per una lista completa della funzioni presenti e per altre informazioni utili, visitare il `manuale ufficiale di QGIS <https://docs.qgis.org/2.14/en/docs/user_manual/working_with_vector/expression.html>_`

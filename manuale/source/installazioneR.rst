.. _installazioneR:

=====================
Ambiente statistico R
=====================
R è un software statistico molto robusto e completamente open source. Viene utilizzato in campi scientifici molto diversi fra loro (matematica pura, statistica, scienze ambientali, genetica, etc.) grazie all'utilizzo di pacchetti esterni chiamati **librerie**.

Queste librerie sono un insieme di algoritmi specifici (dai pacchetti che grafici a pacchetti di geostatistica) che l'utente può liberamente scaricare ed utilizzare.


Installazione
=============
Essendo R un software open source è liberamente scaricabile e installabili su tutti i sistemi operativi (Linux, Windows e OSX).

La procedura di installazione iniziale cambia a seconda del sistema operativo, mentre i comandi di R hanno sempre la stessa sintassi.


Linux
-----
Per le distribuzioni Ubuntu/Debian è sufficiente aprire un terminale e digitare::

  sudo apt-get install r-base r-dev

Dopo aver inserito la password R verrà installato sul proprio computer e sarà accessibile da riga di comando digitando in un terminale semplicemnte `R`.


Windows
-------
Il `sito <https://cran.r-project.org/bin/windows/base/>`_ permette di scaricare la versione di R per Windows. Di seguito viene illustrata la procedura di installazione:

1. **Download di R dal sito**. Cliccare sul link, salvare ed eseguire il file. Inzierà cosi la procedura di installazione:

.. figure:: ../immagini/installazioneR/inst.*
  :scale: 80%
  :align: center

2. **Inizio procedura di installazione**. Alla prima schermata cliccare su **Avanti**:

.. figure:: ../immagini/installazioneR/inst2.*
  :scale: 80%
  :align: center

3. **Accettare i termini di licenza** cliccando su **Avanti**:

.. figure:: ../immagini/installazioneR/inst3.*
  :scale: 80%
  :align: center

4. **Scegliere la cartella di installazione**. L'installer sceglie automaticamente una cartella, se si desidera cambiarla cliccare su **Sfoglia** e inserire il percorso desiderato. Cliccare infine su **Avanti**:

.. _pathR:

.. figure:: ../immagini/installazioneR/inst4.*
  :scale: 80%
  :align: center

5. **Selezionare i file da installare**. Anche in questo caso l'installer spunta automaticamente le caselle dei file da installare (scelta consigliata). Eventualmente spuntare/despuntare altre caselle e cliccare **Avanti**:

.. figure:: ../immagini/installazioneR/inst5.*
  :scale: 80%
  :align: center

6. **Opzioni di avvio**, lasciare i valori predefiniti e cliccare su **Avanti**:

.. figure:: ../immagini/installazioneR/inst6.*
  :scale: 80%
  :align: center

7. **Selezione cartella di Avvio**, anche in questo caso lasciare i valori automaticamente riempiti e cliccare su **Avanti**:

.. figure:: ../immagini/installazioneR/inst7.*
  :scale: 80%
  :align: center

8. **Processi addizionali**, in questa finestra è possibile scegliere se creare icone aggiuntive e come impostare i valori di registro. È consigliato lasciare spuntate le caselle predefinite e avviare cosi l'installazione cliccando sul tasto **Avanti**:

.. figure:: ../immagini/installazioneR/inst8.*
  :scale: 80%
  :align: center

9. **Installazione in corso**. Attendere alcuni istanti affinché l'installazione si completi:

.. figure:: ../immagini/installazioneR/inst9.*
  :scale: 80%
  :align: center

10. **Fine installazione**. Questa finestra informa che il processo di installazione è stato completato ed è ora possibile utilizzare correttamente R:

.. figure:: ../immagini/installazioneR/inst10.*
  :scale: 80%
  :align: center



.. _recoverRpath:

Recupero percorso installazione di R
====================================
Per recuperare rapidamente e facilmente il percorso dell'eseguibile di R è sufficiente cliccare sul menu **Start** e cercare il programma inserendo nell'apposita casella il nome **R** (punto 1 della figura).
Se il programma è stato correttamente installato comparirà nella lista dei risultati (punto 2 della figura). A questo punto cliccare con il tasto destro sul programma e scegliere il menu **Proprietà** (punto 3 della figura):


.. figure:: ../immagini/installazione/inst11.*
  :scale: 80%
  :align: center

Nella finestra che si aprirà, nella scheda **Scorciatoie** compare il percorso completo dell'eseguibile. Tenere a mente il percorso, infatti sarà necessario inserirlo all'interno delle opzioni di *Processing* in QGIS. Il percorso è lo stesso che è stato scelto durante l'installazione di R, vedi :ref:`sezione dedicata <pathR>`

.. figure:: ../immagini/installazione/inst12.*
  :scale: 80%
  :align: center


Risoluzione permessi di scrittura
=================================
Molto raramente, può capitare che gli script non funzionino perché una cartella di sistema di R non è abilitata alla scrittura, è quindi necessario cambiare i permessi:

  * la cartella incriminata di solito è ``C:\Users\tuo_nome_utente\Program Files\R\R-3.2.0\library``
  * tasto destro sulla cartella `library`, "Properties" e scheda "Security"
  * da qui abilitare i permessi (tutti) per i vari utenti

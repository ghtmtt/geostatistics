.. _installazione_pacchetti:

Installazione pacchetti di R
============================
``Processing`` installa automaticamente i pacchetti di R se questi non sono disponibili. Tuttavia, se il pacchetto da installare ha dimensioni notevoli, l'esecuzione dell'algoritmo causa un *crash* di Processing ed è necessario installare i pacchetti direttamente dentro l'ambiente di R.

Avviando R (sia in Windows che in Linux) è possibile utilizzare una console dei comandi dove la sintassi è la stessa.

Digitare **uno alla volta** i seguenti comandi::

  install.packages("rgdal")
  install.packages("sp")
  install.packages("raster")
  install.packages("gstat")
  install.packages("ggplot2")

dopo aver premuto invio, verrà chieso di scegliere un *CRAN mirror*, ovvero un server al quale connettersi per scaricare i pacchetti. Dalla lista selezionare Italia e premere ``OK``. Dopo alcuni instanti partirà il download e  il pacchetto verrà installato.

.. figure:: ../immagini/installazione/inst5.*
  :scale: 80%
  :align: center

  schermata di R dal terminale di Linux

Ripetere la procedura per ogni pacchetto (``sp``, ``raster``, ``gstat``, ``rgdal``, ``ggplot2``)

.. note:: è possibile che alcuni pacchetti siano già presenti con R, in questo caso il pacchetto verrà semplicemente aggiornato (se è disponibile un aggiornamento) oppure ignorato.

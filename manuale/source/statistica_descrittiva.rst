.. _statistica_decrittiva:

======================
Statistica descrittiva
======================
Prima di iniziare ad effettuare delle analisi statistiche sui dati è importante capire la natura dei dati, ovvero come questi si comportano, come sono distribuiti e se hanno dei valori anomali.

Il set di script contiene diversi algoritmi in grado di studiare l'andamento dei dati.

Caricamento dati in QGIS
------------------------
Dopo aver avviato QGIS Desktop, è necessario caricare i dati. Se il dato che si vuole caricare è **vettoriale** cliccare sul pulsante |addVector|, cliccare su sfoglia e cercare i dati (estensione **.shp**) nel computer. Trovati è sufficiente fare doppio click sul dato e poi cliccare su Open.

.. figure:: ../immagini/statistica_descrittiva/add_data.*
  :scale: 100%
  :align: center

Cliccando nuovamente su ``Open`` i dati appariranno nella vista mappa di QGIS.

.. figure:: ../immagini/statistica_descrittiva/add_data2.*
  :scale: 80%
  :align: center

Una volta che i dati sono stati aggiuti è possibile eseguire tutti gli script.

Avvio algoritmi
---------------
Tutti gli algoritmi sviluppati per questi esercizi sono presenti nel menu ``R scripts - [R-Geostatistics]`` all'interno di ``Processing Toolbox``.

Per avviare gli algoritmi è sufficiente filtrarli con il nome nella casella di testo e effettuare doppio click una volta trovato (nell'esempio di seguito, l'algoritmo cercare è ``Summary Statistics``)

.. figure:: ../immagini/statistica_descrittiva/run_alg.*
  :scale: 80%
  :align: center

Ogni volta  che un algoritmo viene lanciato si apre una finestra dove è possibile inserire i vari parametri richiesti dall'algoritmo (1) e decidere se gli output verranno salvati come file temporanei o come file su disco (2).
Ogni algorimo ha inoltre una breve descrizione sulla parte destra della finestra principale (3) e una **guida completa** dei parametri nella scheda *Help* (4).

.. figure:: ../immagini/statistica_descrittiva/run_alg2.*
  :scale: 80%
  :align: center

Alcuni algoritmi hanno come risultato dei raster, dei vettori o delle tabelle che vengono caricati automaticamente nella legenda. Se il risultato è invece una *tabella virtuale* o un grafico, questi sono automaticamente visualizzati e, anche se la finestra viene chiusa, rimangono disponibili dal menu ``Processing -> Visualizzatore Risultati`` (i più recenti sono in basso) per tutta la durata della sessione di QGIS.

.. note:: se si vuole salvare su disco la tabella virtuale o il grafico è sufficiente rilanciare l'algoritmo e scegliere il percorso in cui salvare il risultato.

Riassunto dati e distribuzioni
------------------------------
Di seguito vengono presentati gli algoritmi che permettono di studiare il comportamento e la distribuzione del dati.

Descrizione dei dati
""""""""""""""""""""
Ci sono due modi rapidi ed efficaci per ottenere le principali informazioni dei vari campi del dataset.

* **Summary statistics**

  L'algoritmo ``Summary Statistics`` permette di calcolare rapidamente le principali caratteristiche delle singole variabili (**una alla volta**).

  Carcare l'algoritmo ``Summary Statistics`` a avviarlo. Nei menu inserire il nome del layer vettoriale e in seguito della variabile di cui si vogliono calcolare i parametri e cliccare su ``Run`` (nel seguente esempio si è scelta la variabile ``PB``):

  .. figure:: ../immagini/statistica_descrittiva/summary.*
    :scale: 80%
    :align: center

  in pochi secondi verrà visualizzato l'elenco delle varie variabili calcolate:

  .. figure:: ../immagini/statistica_descrittiva/summary2.*
    :scale: 80%
    :align: center

* **Statistiche di QGIS**

  QGIS ha un algoritmo che permette di calcolare rapidamente le caratteristiche principlai dei campi dei vari vettori caricati. Premendo il pulsante |sum| nella barra in alto si accede a una scheda in cui è possibile scegliere il vettore e il rispettivo campo di cui si vuole vedere un riassunto dei dati:

  .. figure:: ../immagini/statistica_descrittiva/summary3.*
    :scale: 80%
    :align: center

  Maggiori informazioni direttamente sulla `documentazione ufficiale di QGIS <http://docs.qgis.org/2.14/en/docs/user_manual/introduction/qgis_gui.html?highlight=statistical#statistical-summary-panel>`_.


Test di normalità
"""""""""""""""""
L'algoritmo ``normality`` effettua un rapido e semplice test (Shapiro - Wilk) che permette di verificare se la variabile ha una distribuzione normale. L'algoritmo restituisce una tabella virtuale con la *statistica di Wilks*, **W** e il **p-valore**. Nelle seguenti figure la finestra dell'algoritmo e il risultato:

.. figure:: ../immagini/statistica_descrittiva/normality.*
  :scale: 80%
  :align: center

|

.. figure:: ../immagini/statistica_descrittiva/normality2.*
  :scale: 80%
  :align: center


Matrice di Correlazione
"""""""""""""""""""""""
La matrice di correlazione è un metodo che permette di capire molto velocemente la correlazione fra le variabili interessate. I valori, come in ogni correlazione, assumono valori da -1 (inversamente correlati) passando per 0 (per niente correlati) ad 1 (direttamente correlati).

L'algoritmo ``Correlation Matrix`` filtra esclusivamente i campi numerici del vettore in input e crea una tabella automaticamente caricata in legenda e salvabile eventualmente come file csv.

.. figure:: ../immagini/statistica_descrittiva/correlationmatrix.*
  :scale: 80%
  :align: center

La tabella risultante è una **matrice triangolare di correlazione** ovvero i valori visualizzati sono speculari (lo Zn è correlato con il Pb e allo stesso modo il Pb è correlato con lo Zn).

Aprendo la tabella degli attributi si possono vedere i valori di correlazione dei vari campi del vettore in input:

.. figure:: ../immagini/statistica_descrittiva/correlationmatrix2.*
  :scale: 80%
  :align: center


Grafici
"""""""
Per la creazione dei grafici è stato usato il pacchetto di R ``ggplot2``.

Istogramma
^^^^^^^^^^
L'algoritmo ``HistogramGG`` calcola un semplcie istogramma per studiare la distribuzione dei dati. È possibile specificare un campo opzionale se è presente una variabile categorizzata che si vuole usare per suddividere i dati (ad esempio, diverse categorie in funzione del tipo di suolo).

Esempio di istogramma sìcon semplice variabile continua:

.. figure:: ../immagini/statistica_descrittiva/hist.*
  :scale: 80%
  :align: center

Sul grafico vengono anche mostrate 3 linee verticali relative alla *media*, *mediana* e *deviazione standard*:

.. figure:: ../immagini/statistica_descrittiva/hist2.*
  :scale: 80%
  :align: center

Esempio di istogramma con l'aggiunta di una variabili categorizzata:

.. figure:: ../immagini/statistica_descrittiva/hist3.*
  :scale: 80%
  :align: center

Viene assegnato un colore ad ogni diversa categoria che viene indicata in legenda

.. figure:: ../immagini/statistica_descrittiva/hist4.*
  :scale: 80%
  :align: center


Scatter plot con retta di regressione
"""""""""""""""""""""""""""""""""""""
L'algoritmo ``ScatterplotGG`` permette di creare un semplice grafico a dispersione aggiungeno opzionalmente una retta di regressione che meglio interpola i dati.

.. figure:: ../immagini/statistica_descrittiva/scatter.*
  :scale: 80%
  :align: center

Scegliere le due variabili di cui si vuole creare il grafico ed eventualmente spuntare la casella ``Regression``.

.. figure:: ../immagini/statistica_descrittiva/scatter2.*
  :scale: 80%
  :align: center

Sul grafico viene mostrata la retta di regressione con l'intervallo di confidenza (al 95%) e il valore di R2 per stimare la bontà della regressione.


Boxplot
"""""""
L'algoritmo ``BoxplotGG`` permette di visualizzare un boxplot, molto utile per studiare la distribuzione dei dati ed individuare eventuali outliers. L'algoritmo permette di scegliere un campo opzionale di raggruppamento dei valori (variabile categorizzata) e di trasformare i dati con diversi operatori (log10, ln, radice quadrata, esponenziale).


.. figure:: ../immagini/statistica_descrittiva/boxplot.*
  :scale: 80%
  :align: center

Dopo aver scelto i parametri i boxplot risultanti saranno visualizzati nel `Visualizzatore Risultati` di Processing. Confronto fra distribuzione di una variabile senza trasformazione dei dati e con trasformazione logaritmica in base 10:

.. figure:: ../immagini/statistica_descrittiva/boxplot2.*
  :scale: 80%
  :align: center

Se si sceglie una variabile categorizzata i boxplot assumono colorazioni casuali in funzione di ogni categoria:

.. figure:: ../immagini/statistica_descrittiva/boxplot3.*
  :scale: 80%
  :align: center

Densità
"""""""
L'algoritmo ``DensityGG`` crea un grafico di densità della distribuzione di una variabile. L'algoritmo permette di scegliere la variabile in input e opzionalmente una variabile categorica con cui suddividere i dati.

.. figure:: ../immagini/statistica_descrittiva/density.*
  :scale: 80%
  :align: center

Il risultato finale è un grafico di densità che descrive l'andamento dei dati. Se è stata scelta una variabile categorizzata l'algoritmo applica automaticamente un criterio di trasparenza ad ogni curva in modo da rendere visibili i risultati sovrapposti

.. figure:: ../immagini/statistica_descrittiva/density2.*
  :scale: 80%
  :align: center

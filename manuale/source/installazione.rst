.. _installazione:

============
Prerequisiti
============
QGIS e R sono entrambi software open source e multipiattaforma. Questi software devono essere installati sul proprio computer affinché gli algoritmi funzionino correttamente.

Una delle forze di QGIS è quella di poter usare algoritmi di altri software all'interno della stessa interfaccia grafica.

Tutto questo è possibile grazie al plugin nativo **Processing**.

Configurazione software esterni
===============================
Alcuni software esterni a QGIS, come GRASS e SAGA, sono già preconfigurati e pronti all'utilizzo senza che l'utente debba fare niente.

Questo non accade con R. Infatti R, essendo un software statistico, non è incluso nell'installazione di QGIS, e deve essere scaricato ed installato a cura dell'utente. Per l'installazione di **R** vedi il capitolo dedicato :ref:`installazioneR`.


Dati e algoritmi
================
Insieme a questo manuale viene fornita il file zip **dati** con all'interno un dataset di esempio contenuto nella cartella **dataset** (dati puntuali con valori casuali di inquinanti) e un raster da usare come sfondo. Oltre alla cartella **dataset** è presente anche la cartella **algoritmi** che contiene tutti gli algoritmi geostatisici da utilizzare in QGIS.


Aggiunta degli algoritmi di R in QGIS
=====================================
Gli algoritmi di R per QGIS sono dei file di testo molto leggeri scritti in modo che il plugin *Processing* sia in grado di elaborarli.

È necessario aggiugere i file contentuti nella cartella **algoritmi** presente nello zip **dati** in una specifica cartella di QGIS (che non invalida l'installazione né può causare nessun danno).

I percorsi sono leggermente diversi a seconda del sistema operativo.

.. warning:: è necessario che QGIS sia stato avviato **almeno una volta** dopo essere stato installato. Questo in quanto al primo avvio viene creata una struttura di cartelle che vengono lette ad ogni futuro avvio di QGIS.


Windows
-------
La cartella in cui incollare gli algoritmi è ``C:\Users\tuo_nome_utente\.qgis2\processing\rscripts``:

.. figure:: ../immagini/installazione/inst13.*
  :scale: 80%
  :align: center

Copiare gli algoritmi contenuti nella cartella **algoritmi** fornita **all'interno della cartella rscripts**.


Linux
-----
In Linux la cartella di QGIS in cui incollare gli algorimi della cartella fornita **algoritmi** all'interno di ``.qgis2/processing/rscripts`` presente nella ``home`` dell'utente.


Impostazioni di Processing in QGIS
==================================
Prima di attivare le impostazioni di *Processing* all'interno di QGIS, per gli utenti Windows è bene recuperare il percorso in cui è presente l'eseguibile di R. Per recuperare il percorso dell'eseguible di R vedi :ref:`sezione dedicata <recoverRpath>`.

A seconda delle opzioni scelte durante l'installazione di R i percorsi in cui è presente l'eseguible possono cambiare.

Avviare quindi QGIS e cliccare sul menu ``Processing -> Opzioni`` nella barra degli strumenti:

.. figure:: ../immagini/installazione/inst1.*
  :scale: 80%
  :align: center

|

.. note:: il plugin *Processing* dovrebbe comparire automaticamente nella barra dei menu. In caso contrario è necessario attivarlo dal menu `Plugins -> Gestisci e Installa Plugin` filtrare nell apposita casella il nome *Processing* e spuntare la casella. Cliccare poi su **OK**

espandere poi il menu **Programmi** e in seguito il menu **R scripts**:

.. figure:: ../immagini/installazione/inst2.*
  :scale: 80%
  :align: center

|

Per gli utenti *Windows* è necessario impostare il percorso dell'eseguibile di R. Nel menu **R scripts** eseguire le seguenti impostazioni:

* attivare la casella **Activate**

.. figure:: ../immagini/installazione/instRwin1.*
  :scale: 80%
  :align: center

* fare doppio click accanto alla casella **Cartella R**. In questo modo è possibile inserire il percorso all'eseguibile di R. Cliccare sul pulsante |browse|


.. figure:: ../immagini/installazione/instRwin2_1.*
  :scale: 80%
  :align: center

Nella finestra di dialogo che si aprirà, scegliere il percorso corretto dell'eseguibile di R (per capire qual è il percorso corretto da inserire, vedi :ref:`sezione dedicata <recoverRpath>`)

.. warning:: **non cliccare sull'eseguibile di R, ma limitarsi ad inserire la cartella in cui è presente!**

.. figure:: ../immagini/installazione/instRwin2.*
  :scale: 80%
  :align: center


Se tutti i parametri sono stati impostati correttamente, i percorsi e le diverse cartelle **devono** comparire come nella seguente finestra:

.. figure:: ../immagini/installazione/instRwin3.*
  :scale: 80%
  :align: center

|

dove:

* `Activate` attiva gli algoritmi di R dentro *Processing*
* `Cartella script R` è la cartella in cui *Processing* carica gli script presenti
* `Cartella R` è la cartella dove sono presenti gli eseguibili del programma
* `Cartella della libreria R` è la cartella in cui *Processing* carica librerie aggiuntive di R
* `Usa la versione 64 bit` casella di controllo da attivare se si vuole usare la versione a 64 bit di R



Per gli utenti Linux invece il percorso dell'eseguibile è in una cartella predefinita che Processing legge automaticamente è quindi sufficiente spuntare la casella di controllo `Activate`.


Per rendere effettive le modifiche **è necessario riavviare QGIS**. Dopo il riavvio, aprendo il menu di ``Processing -> Toolbox`` gli script sono disponibili nella cartella di R:

.. figure:: ../immagini/installazione/inst4.*
  :scale: 80%
  :align: center

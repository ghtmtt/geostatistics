##[R-Geostatistics]=group
##showplots
##Layer=vector
##X=Field Layer
##Group=optional Field Layer
#Log_Scale = boolean False
##Data_Transformation=selection normal;log10;ln;sqrt;exp

require(ggplot2)


if(Data_Transformation == 0){
ax <- scale_y_continuous()
yy <- ylab(X)
} else if(Data_Transformation == 1){
ax <- scale_y_log10()
yy <- ylab(paste("log10", X))
} else if(Data_Transformation == 2){
ax <- coord_trans(y = "log")
yy <- ylab(paste("ln", X))
} else if(Data_Transformation == 3){
ax <- coord_trans(y = "sqrt")
yy <- ylab(paste("sqrt", X))
} else if(Data_Transformation == 4){
ax <- coord_trans(y = "exp")
yy <- ylab(paste("exp", X))
}

if(is.null(Group)){
ggplot()+
geom_boxplot(aes(x=1, y=Layer[[X]]))+
xlab("")+
yy +
ax +
ggtitle(paste("Boxplot of", X))
} else {
ggplot()+
geom_boxplot(aes(x=Layer[[Group]], y=Layer[[X]], fill=as.factor(Layer[[Group]])))+
xlab(Group)+
yy +
scale_fill_discrete(name=Group) +
ax +
ggtitle(paste("Boxplot of", X))
}

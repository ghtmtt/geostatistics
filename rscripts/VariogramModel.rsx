##[R-Geostatistics]=group
##layer=vector
##field=field layer
##model=selection Sph;Exp;Gau;Mat
##nugget=number 0
##psill=string auto
##range=string auto
##showplots


library(gstat)
library(sp)
library(ggplot2)

Models<-c("Sph","Exp","Gau","Mat")
model2<-Models[model+1]

# remove duplicates and null values before running the variogram
layer <- subset(layer, !is.na(layer[[field]]))


# function to get the right output
transform <- function(ps, ran, nug, mod){
if(ps=="auto" && ran=="auto"){
ps ="auto"
ran="auto"
vario = variogram(layer[[field]]~1, layer)
varmod = fit.variogram(vario, vgm(mod))
} else if(ps=="auto" && ran!="auto") {
ps ="auto"
ran=as.numeric(ran)
vario = variogram(layer[[field]]~1, layer)
varmod = vgm(nugget=nug, range=ran, model=model2)
} else if(ps!="auto" && ran=="auto"){
ran ="auto"
ps=as.numeric(ps)
vario = variogram(layer[[field]]~1, layer)
varmod = vgm(nugget=nug, psill=ps, model=model2)
} else {
ps=as.numeric(ps)
ran=as.numeric(ran)
vario = variogram(layer[[field]]~1, layer)
varmod = vgm(nugget=nug, psill=ps, range=ran, model=model2)
}
l <-list(varmod, vario)
}


# run the function and extract the output
complete_list <- transform(psill, range, nugget, model2)
kri <- complete_list[[1]]
var <- complete_list[[2]]

# console output for the chosen model (psill and range)
>kri

#plot variogram and variogram model with ggplot

# get the variogram fitting line (for gggplot)
lines <- variogramLine(kri, maxdist=max(var$dist))

# ggplot
ggplot(var, aes(x = dist, y = gamma)) +
geom_text(aes(label=np, size=3, vjust = -1)) +
geom_line(data = lines) +
geom_point() +
theme(legend.position="none") +
ggtitle(paste("Variogram Model of", field))

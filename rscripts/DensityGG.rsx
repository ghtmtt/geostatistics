##[R-Geostatistics]=group
##showplots
##Layer=vector
##X=Field Layer
##Group=optional Field Layer

require(ggplot2)

if(is.null(Group)){
ggplot()+
geom_density(aes(x=Layer[[X]]))+
xlab("")+
ylab(X) +
ggtitle(paste("Density plot of", X))
} else {
ggplot()+
geom_density(aes(x=Layer[[X]], fill=as.factor(Layer[[Group]]), position="stack"), alpha=0.2)+
xlab("")+
ylab(X)+
scale_fill_discrete(name=Group) +
ggtitle(paste("Density plot of", X))
}

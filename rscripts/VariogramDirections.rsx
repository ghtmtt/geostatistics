##[R-Geostatistics]=group
##showplots
##layer=vector
##field=field layer
##range = optional string auto
##alpha1=optional string 0
##alpha2=optional string 45
##alpha3=optional string 90
##alpha4=optional string 135
require(gstat)
require(ggplot2)

#replace spaces in the field names with point
layer@data <- data.frame(lapply(layer@data, trimws))

# repalce the field names for matching
field <- gsub("\\s", ".", field)

# remove duplicates and null values before running the variogram
layer <- subset(layer, !is.na(layer[[field]]))

# hack for the 4 directions
if(alpha1==""){
a1<-NULL
} else {
a1<-as.numeric(alpha1)
}

if(alpha2==""){
a2<-NULL
} else {
a2<-as.numeric(alpha2)
}

if(alpha3==""){
a3<-NULL
} else {
a3<-as.numeric(alpha3)
}

if(alpha4==""){
a4<-NULL
} else {
a4<-as.numeric(alpha4)
}

# compute the variogram
if(range=="auto"){
#compute the variogram with automatic cutoff and alphas
var <- variogram(layer[[field]]~1, layer, alpha=c(a1,a2, a3, a4))
} else {
cutoff2 <- as.numeric(range)
#compute the variogram with specified cutoff and alphas
var <- variogram(layer[[field]]~1, layer, cutoff=cutoff2, alpha=c(a1,a2, a3, a4))
}



ggplot(var, aes(x=dist, y=gamma, color=factor(dir.hor), label=np)) +
geom_point() +
geom_text(size=3, vjust = -1) +
geom_line() +
scale_color_discrete(name="Directions") +
ggtitle(paste("Multi Directional Variogram of", field))

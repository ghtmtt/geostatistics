##[R-Geostatistics]=group
##showplots
##Layer=vector
##X=Field Layer
##Group=optional Field Layer

require(ggplot2)

#stat parameters
mea <- round(mean(Layer[[X]], na.rm=T),2)
med <- round(median(Layer[[X]], na.rm=T),2)
st <- round(sd(Layer[[X]], na.rm=T),2)


if(is.null(Group)){
p <- ggplot()+
geom_histogram(aes(x=Layer[[X]]))+
geom_vline(aes(xintercept=mean(Layer[[X]], na.rm=T), color="blue"))+
geom_vline(aes(xintercept=median(Layer[[X]], na.rm=T), color="green"))+
geom_vline(aes(xintercept=(mean(Layer[[X]], na.rm=T) + sd(Layer[[X]], na.rm=T)), color="red"))+
geom_vline(aes(xintercept=(mean(Layer[[X]], na.rm=T) - sd(Layer[[X]], na.rm=T)), color="red"))+
scale_colour_manual(values = c("blue", "green", "red", "red"), labels=c("mean", "median", "sd"), name="Statistics")+
xlab(Group)+
ylab(paste(X, " count"))+
ggtitle(paste("Histogram of", X))

p + annotate("text", x=max(ggplot_build(p)$data[[1]]$xmax)-2, y=max(ggplot_build(p)$data[[1]]$y), label=paste("mean = ", mea)) +
annotate("text", x=max(ggplot_build(p)$data[[1]]$xmax)-2, y=max(ggplot_build(p)$data[[1]]$y) -2, label=paste("median = ", med)) +
annotate("text", x=max(ggplot_build(p)$data[[1]]$xmax)-2, y=max(ggplot_build(p)$data[[1]]$y) -4 , label=paste("st  = ", st))

} else {
p <- ggplot()+
geom_histogram(aes(x=Layer[[X]], fill=as.factor(Layer[[Group]])))+
scale_fill_discrete(name=Group) +
xlab(Group)+
ylab(paste(X, " count")) +
ggtitle(paste("Histogram of", X))

p
}

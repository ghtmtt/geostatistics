##[R-Geostatistics]=group
##showplots
##layer=vector
##field=field layer
##range = optional string auto

require(gstat)
require(ggplot2)

#replace spaces in the field names with point
layer@data <- data.frame(lapply(layer@data, trimws))

# repalce the field names for matching
field <- gsub("\\s", ".", field)

# remove duplicates and null values before running the variogram
layer <- subset(layer, !is.na(layer[[field]]))


if(range=="auto"){
#compute the variogram with automatic cutoff
var <- variogram(layer[[field]]~1, layer)
} else {
cutoff2 <- as.numeric(range)
#compute the variogram
var <- variogram(layer[[field]]~1, layer, cutoff=cutoff2)
}


# plot variogram with ggplot
ggplot(var, aes(x=dist, y=gamma, label=np)) +
geom_point() +
geom_text(size=3, vjust = -1) +
geom_line() +
geom_hline(aes(yintercept=var(layer[[field]], na.rm=T), color="red")) +
scale_color_manual(values=c("red"), labels=c("variance"), name="") +
ggtitle(paste("Experimental Variogram of", field))

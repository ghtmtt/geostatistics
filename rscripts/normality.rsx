##[R-Geostatistics]=group
##showplots
##Layer=vector
##Variable=Field Layer

# run shapiro test
sh<-shapiro.test(Layer[[Variable]])

# style shapiro output
ch<-data.frame(rbind(sh$method,
signif(sh$statistic, digits=3),
signif(sh$p.value, digits=3)),
row.names=c("Method: ", "W = ", "p-value: "))
colnames(ch)<-c(paste("Normality Test of ", Variable))

# console output of shapiro
>ch


# ggplot
require(ggplot2)

#data preparation
df<-as.data.frame(Layer)
FX<-Layer[[Variable]]

#Find QQ-line parameters
# Find the 1st and 3rd quartiles
y <- quantile(FX, c(0.25, 0.75), na.rm=T)
# Find the matching normal values on the x-axis
x <- qnorm( c(0.25, 0.75))
# Compute the line slope
slope <- diff(y) / diff(x)
# Compute the line intercept
int <- y[1] - slope * x[1]

#Plot QQline + QQPlot
ggplot(df) +
stat_qq(aes(sample=FX))+
geom_abline(intercept=int, slope=slope)
ylab(Variable)

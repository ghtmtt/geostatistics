##[R-Geostatistics]=group
##layer=vector
##field=field layer
##cutoff=number 1500
##width=number 100
##showplots


library("gstat")
library("sp")
library("ggplot2")

#replace spaces in the field names with point
layer@data <- data.frame(lapply(layer@data, trimws))

# repalce the field names for matching
field <- gsub("\\s", ".", field)


# remove duplicates and null values before running the variogram
layer <- subset(layer, !is.na(layer[[field]]))

# compute the variogram
var <- variogram(layer[[field]]~1, layer, cutoff=cutoff, width=width, map=TRUE)

# transform the data for ggplot
df <- as.data.frame(var$map)

ggplot(df, aes(dx, dy))+
geom_raster(aes(fill=var1)) +
xlab("X")+
ylab("Y")+
scale_fill_gradientn(colours = terrain.colors(10), na.value="white", name=paste("Variance of ", field)) +
ggtitle(paste("Variogram Map of", field))


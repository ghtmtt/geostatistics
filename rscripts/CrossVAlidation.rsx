##[R-Geostatistics]=group
##layer=vector
##field=field layer
##model=selection Sph;Exp;Gau;Mat
##nugget=number 0
##insert_psill_manually=boolean False
##psill=number 0
##insert_range_manually=boolean False
##range=number 0
##showplots
#Cross_validation= output table


library('gstat')
library('sp')
library(ggplot2)
library(gridExtra)

# define the model list
Models<-c("Sph","Exp","Gau","Mat")
model2<-Models[model+1]

# remove duplicates and null values before running the variogram
names(layer)[names(layer)==field]="field"
layer$field <- as.numeric(as.character(layer$field))
layer <- remove.duplicates(layer)
layer <- layer[!is.na(layer$field),]

# variogram formula and variogram calculation
g = gstat(id = field, formula = field~1, data = layer)
vg = variogram(g)

if(!(insert_psill_manually)){psill=NA}
if(!(insert_range_manually)){range=NA}

#variogram model
vgm = vgm(nugget=nugget, psill=psill, range=range, model=model2)
vgm = fit.variogram(vg, vgm)

#console output
#>print(vgm[2,], row.names=F)

# prediction map
prediction = krige.cv(field~1, layer, vgm)
#prediction@data
k <- as.data.frame(prediction)

# residuals error
res <- as.data.frame(prediction)$residual

errors <- data.frame(rbind(mean(res),
mean(res^2/as.data.frame(k)$var1.var)),
row.names=c("Mean Error (ideally is 0): ", "Variance Error (ideally is 1): "))
colnames(errors)<-c(field)

>errors



p1<-ggplot()+
geom_point(data=subset(k, k$zscore<=1.96 & k$zscore>=-1.96), aes(var1.pred,zscore, color="green")) +
geom_point(data=subset(k, k$zscore>=1.96 | k$zscore<=-1.96), aes(var1.pred,zscore, color="red")) +
geom_hline(yintercept = 1.96, linetype="dashed")+
geom_hline(yintercept = -1.96, linetype="dashed") +
geom_hline(yintercept = 0)+
xlab("Predicted Variance")+
scale_color_manual(values=c("green", "red"), name="", labels=c("normal", "outliers"))+
theme(legend.position="none")

p2 <- ggplot()+
geom_histogram(data=subset(k, k$zscore<=1.96 & k$zscore>=-1.96),
aes(x=zscore, fill="green")) +
geom_histogram(data=subset(k, k$zscore>=1.96 | k$zscore<=-1.96),
aes(x=zscore, fill="red")) +
geom_vline(xintercept = 1.96, linetype="dashed")+
geom_vline(xintercept = -1.96, linetype="dashed") +
scale_fill_manual(values=c("green", "red"), name="", labels=c("normal", "outliers"))+
theme(legend.position="none")

p3 <- ggplot()+
geom_abline(intercept = 0, slope=1) +
geom_point(data=subset(k, k$zscore<=1.96 & k$zscore>=-1.96),
aes(observed,var1.pred, color="green")) +
geom_point(data=subset(k, k$zscore>=1.96 | k$zscore<=-1.96),
aes(observed,var1.pred, color="red"))+
scale_color_manual(values=c("green", "red"), name="", labels=c("normal", "outliers"))+
xlab("Values Observed")+
ylab("Values Predicted")+
theme(legend.position="none")

p4 <- ggplot()+
geom_point(data=subset(k, k$zscore<=1.96 & k$zscore>=-1.96),
aes(coords.x1,coords.x2, color="green")) +
geom_point(data=subset(k, k$zscore>=1.96 | k$zscore<=-1.96),
aes(coords.x1,coords.x2, color="red"))+
scale_color_manual(values=c("green", "red"), name="", labels=c("normal", "outliers"))+
xlab("x")+
ylab("y")+
theme(legend.position="none")

grid.arrange(p1, p2, p3, p4, ncol=2)

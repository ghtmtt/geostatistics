##[R-Geostatistics]=group
##layer=vector
##field=field layer
##Extent = extent
##model=selection Sph;Exp;Gau;Mat
##nugget=number 0
##insert_psill_manually=boolean False
##psill=number 0
##insert_range_manually=boolean False
##range=number 0
##Resolution=number 1000
##showplots
##Variance_Map= output raster
##Prediction_Map= output raster

library(gstat)
library(sp)
library(ggplot2)

Models<-c("Sph","Exp","Gau","Mat")
model2<-Models[model+1]


create_new_data_sel <- function (layer, ext){
bottomright <- c(Extent@xmin, Extent@ymin)
topleft <- c(Extent@xmax, Extent@ymax)
d <- SpatialPolygons(
list(Polygons(list(Polygon(coords = matrix(
c(topleft[1],bottomright[1], bottomright[1],topleft[1],topleft[1],
topleft[2], topleft[2], bottomright[2],
bottomright[2],topleft[2]), ncol=2, nrow= 5))), ID=1)))
if(is.projected(layer) & Resolution != 0){
new_data = spsample(d, n= 1, cellsize=c(Resolution,Resolution),
type="regular")}
gridded(new_data) = TRUE
attr(new_data, "proj4string") <-layer@proj4string
return(new_data)
}

mask<-create_new_data_sel (layer, Extent)
names(layer)[names(layer)==field]="field"
layer$field <- as.numeric(as.character(layer$field))
# remove duplicates and null values before running the variogram
layer <- remove.duplicates(layer)
layer <- layer[!is.na(layer$field),]

g = gstat(id = field, formula = field~1, data = layer)
vg = variogram(g)

if(!(insert_psill_manually)){psill=NA}
if(!(insert_range_manually)){range=NA}

#variogram model
vgm = vgm(nugget=nugget, psill=psill, range=range, model=model2)
vgm = fit.variogram(vg, vgm)
#console output
>print(vgm[2,], row.names=F)

# prediction map
prediction = krige(field~1, layer, newdata = mask, vgm)

#plot variogram and variogram model with ggplot

# get the variogram fitting line (for gggplot)
lines <- variogramLine(vgm, maxdist=max(vg$dist))

# ggplot
ggplot(vg, aes(x = dist, y = gamma)) +
geom_text(aes(label=np, size=3, vjust = -1)) +
geom_line(data = lines) +
geom_point() +
theme(legend.position="none")


#kriging maps
Variance_Map = raster(prediction)
Prediction_Map = raster(prediction["var1.var"])
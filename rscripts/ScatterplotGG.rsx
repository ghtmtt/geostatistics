##[R-Geostatistics]=group
##showplots
##Layer=vector
##X=Field Layer
##Y=Field Layer
##Group=optional Field Layer
##Regression = boolean False

require(ggplot2)


#ggplot(data=data.frame(x=Layer[[X]], y=Layer[[Y]]), aes(x, y)) +
#geom_point()

fit <- lm(Layer[[Y]]~Layer[[X]])
r2 <-round(summary(fit)$r.squared, 2)

#title to call
tit = ggtitle(paste("Scatterplot of", X, "vs", Y))

#no regression
if(!Regression){
if(is.null(Group)){
ggplot(data=data.frame(x=Layer[[X]], y=Layer[[Y]]), aes(x, y)) +
geom_point() +
xlab(X)+
ylab(Y) +
tit
} else {
ggplot(data=data.frame(x=Layer[[X]], y=Layer[[Y]], g = Layer[[Group]]), aes(x, y, color=as.factor(g))) +
geom_point()+
xlab(X)+
ylab(Y)+
scale_color_discrete(name=Group) +
tit
}
#yes regression
} else {
if(!is.null(Group)){
ggplot(data=data.frame(x=Layer[[X]], y=Layer[[Y]], g = Layer[[Group]]), aes(x, y, color=as.factor(g))) +
geom_point() +
geom_smooth(method="lm")+
xlab(X)+
ylab(Y) +
scale_color_discrete(name=Group) +
tit
} else {
p <- ggplot(data=data.frame(x=Layer[[X]], y=Layer[[Y]]), aes(x, y)) +
geom_point() +
geom_smooth(method="lm")+
xlab(X)+
ylab(Y)
xx <- head(sort(Layer[[X]], decreasing = F))[1]
yy <- head(sort(Layer[[Y]], decreasing = T))[1]
p + annotate("text", x=xx, y=yy, label=paste("R2 = ", r2), hjust=0) +
tit
}
}
##[R-Geostatistics]=group
##layer=vector
##field=field layer
##Extent = extent
##Pixel_size=number 1000
##model=selection Sph;Exp;Gau;Mat
##nugget=number 0
##psill=string auto
##range=string auto
##showplots
##Variance_Map= output raster
##Prediction_Map= output raster


library(gstat)
library(sp)
library(ggplot2)

Models<-c("Sph","Exp","Gau","Mat")
model2<-Models[model+1]

# remove duplicates and null values before running the variogram
#layer <- subset(layer, !is.na(layer[[field]]))

# remove duplicates and null values before running the variogram
layer <- remove.duplicates(layer)
layer <- subset(layer, !is.na(layer[[field]]))

# function to get the right output
transform <- function(ps, ran, nug, mod){
if(ps=="auto" && ran=="auto"){
vario = variogram(layer[[field]]~1, layer)
varmod = fit.variogram(vario, vgm(mod))
} else if(ps=="auto" && ran!="auto") {
ran=as.numeric(ran)
vario = variogram(layer[[field]]~1, layer)
varmod = vgm(nugget=nug, range=ran, model=model2)
} else if(ps!="auto" && ran=="auto"){
ps=as.numeric(ps)
vario = variogram(layer[[field]]~1, layer)
varmod = vgm(nugget=nug, psill=ps, model=model2)
} else {
ps=as.numeric(ps)
ran=as.numeric(ran)
vario = variogram(layer[[field]]~1, layer)
varmod = vgm(nugget=nug, psill=ps, range=ran, model=model2)
}
l <-list(varmod, vario)
return(l)
}


# run the function and extract the output
complete_list <- transform(psill, range, nugget, model2)
varmod <- complete_list[[1]]
var <- complete_list[[2]]

# console output for the chosen model (psill and range)
>varmod

#plot variogram and variogram model with ggplot

# get the variogram fitting line (for gggplot)
lines <- variogramLine(varmod, varmod$range[2])
lines2 <- variogramLine(varmod, maxdist=max(var$dist))

# ggplot
ggplot(var, aes(x = dist, y = gamma)) +
geom_text(aes(label=np, size=3, vjust = -1)) +
geom_line(data = lines) +
geom_line(data = lines2) +
geom_point() +
theme(legend.position="none") +
ggtitle(paste("Variogram Model of", field))


# output maps

# extent of the map
create_new_data_sel <- function (layer, ext){
bottomright <- c(Extent@xmin, Extent@ymin)
topleft <- c(Extent@xmax, Extent@ymax)
d <- SpatialPolygons(
list(Polygons(list(Polygon(coords = matrix(
c(topleft[1],bottomright[1], bottomright[1],topleft[1],topleft[1],
topleft[2], topleft[2], bottomright[2],
bottomright[2],topleft[2]), ncol=2, nrow= 5))), ID=1)))
if(is.projected(layer) & Pixel_size != 0){
new_data = spsample(d, n= 1, cellsize=c(Pixel_size,Pixel_size),
type="regular")}
gridded(new_data) = TRUE
attr(new_data, "proj4string") <-layer@proj4string
return(new_data)
}

mask<-create_new_data_sel (layer, Extent)

prediction = krige(layer[[field]]~1, layer, newdata = mask, varmod)

#kriging maps
Prediction_Map = raster(prediction)
Variance_Map = raster(prediction["var1.var"])
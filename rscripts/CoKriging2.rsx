##[R-Geostatistics]=group
##showplots
##layer=vector
##field=field layer
##field2=field layer
##Extent = extent
##Resolution=number 1000
#Variance_Map= output raster
#Prediction_Map= output raster


require(gstat)
require(ggplot2)
require(reshape2)

#replace spaces in the field names with point
layer@data <- data.frame(lapply(layer@data, trimws))

# repalce the field names for matching
field <- gsub("\\s", ".", field)
field2 <- gsub("\\s", ".", field2)

# create extend for interpolation
create_new_data_sel <- function (layer, ext){
bottomright <- c(Extent@xmin, Extent@ymin)
topleft <- c(Extent@xmax, Extent@ymax)
d <- SpatialPolygons(
list(Polygons(list(Polygon(coords = matrix(
c(topleft[1],bottomright[1], bottomright[1],topleft[1],topleft[1],
topleft[2], topleft[2], bottomright[2],
bottomright[2],topleft[2]), ncol=2, nrow= 5))), ID=1)))
if(is.projected(layer) & Resolution != 0){
new_data = spsample(d, n= 1, cellsize=c(Resolution,Resolution),
type="regular")}
gridded(new_data) = TRUE
attr(new_data, "proj4string") <-layer@proj4string
return(new_data)
}

mask<-create_new_data_sel (layer, Extent)


# adjust names fo the fields
names(layer)[names(layer)==field]="field"
names(layer)[names(layer)==field2]="field2"

# convert the fields to be sure they are numeric
layer$field <- as.numeric(as.character(layer$field))
layer$field2 <- as.numeric(as.character(layer$field2))


# remove duplicates
layer <- remove.duplicates(layer)

# remove NA values (separated for each field)
layer <- layer[!is.na(layer$field),]
layer <- layer[!is.na(layer$field2),]


# build the gstat object 
g = gstat(NULL, field, field~1, layer)
g = gstat(g, field2, field2~1, layer)

# build the gstat model
g = gstat(g, model = vgm(1, "Sph", 3000, 50), fill.all = TRUE)

# variogram and variogram model
v <- variogram(g)
g.fit <- fit.lmc(v, g)

# run the CoKriging function
cokr = predict(g.fit, mask)



# POST PROCESSING #


# get the df of the cokr
df <- as.data.frame(cokr)

# add coordinates
df <- cbind(df, cokr@coords[,1])
df <- cbind(df, cokr@coords[,2])

#change the names of the coordinates columns
colnames(df)[colnames(df) == "cokr@coords[, 1]"] <- 'X'
colnames(df)[colnames(df) == "cokr@coords[, 2]"] <- 'Y'

# melt the data into a large data frame

dfmelted <- melt(df, id=c("X", "Y", "x1", "x2"))

# build the plot with ggplot and facet_wrap
ggplot(dfmelted, aes(X, Y))+
geom_tile(aes(fill=value)) +
facet_wrap(~variable)
##[R-Geostatistics]=group
##Layer=vector
##correlation_matrix= output table

p <- cor(Layer@data[sapply(Layer@data, is.numeric)], use = "na.or.complete")
correlation_matrix <- p

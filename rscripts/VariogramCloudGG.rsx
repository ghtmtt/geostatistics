##[R-Geostatistics]=group
##showplots
##layer=vector
##field=field layer

library(gstat)
library(sp)
library(ggplot2)

#replace spaces in the field names with point
layer@data <- data.frame(lapply(layer@data, trimws))

# repalce the field names for matching
field <- gsub("\\s", ".", field)

# remove duplicates and null values before running the variogram
layer <- subset(layer, !is.na(layer[[field]]))

# compute variogram
var <- variogram(layer[[field]]~1, layer, cloud=T)

#plot variogram results
ggplot(var, aes(dist, gamma))+
geom_point()+
geom_hline(aes(yintercept=var(layer[[field]], na.rm=T), color="red")) +
xlab("distance")+
ylab("semivariance")+
scale_color_manual(values=c("red"), labels=c("variance"), name="")+
ggtitle(paste("Variogram Cloud", field))
